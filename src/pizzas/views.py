from rest_framework import viewsets


from .models import (
    Ingridient,
    Pizza,
    Customer,
)
from .serializers import (
    IngridientSerializer,
    PizzaSerializer,
    CustomerSerializer,
)


class IngridientViewSet(viewsets.ModelViewSet):
    model = Ingridient
    serializer_class = IngridientSerializer
    queryset = Ingridient.objects.all()


class PizzaViewSet(viewsets.ModelViewSet):
    model = Pizza
    serializer_class = PizzaSerializer
    queryset = Pizza.objects.all()


class CustomerViewSet(viewsets.ModelViewSet):
    model = Customer
    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()
