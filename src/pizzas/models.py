from django.db import models


class Ingridient(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Pizza(models.Model):
    name = models.CharField(max_length=100)
    price = models.FloatField()
    ingridients = models.ManyToManyField(Ingridient, related_name='pizzas')

    def __str__(self):
        return self.name


DISCOUNT_5 = 1
DISCOUNT_15 = 2
DISCOUNT_25 = 3
DISCOUNTS = (
    (DISCOUNT_5, '5 %'),
    (DISCOUNT_15, '15 %'),
    (DISCOUNT_25, '25 %'),
)


class Customer(models.Model):
    name = models.CharField(max_length=100)
    order = models.ForeignKey(Pizza)
    timestamp = models.DateTimeField(auto_now_add=True)
    discount = models.IntegerField(choices=DISCOUNTS)

    def __str__(self):
        return self.name
