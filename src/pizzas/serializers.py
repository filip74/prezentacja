from .models import (
    Ingridient,
    Pizza,
    Customer,
)

from rest_framework import serializers


class IngridientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ingridient
        fields = (
            'name',
        )


class PizzaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pizza
        fields = (
            'name',
            'price',
            'ingridients',
        )


class CustomerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Customer
        fields = (
            'name',
            'order',
            'timestamp',
            'discount',
        )
